import subprocess


# Visual indentation in function signature => warning
def x(a, b, c,
      d, e, f):
    pass


# No indentation in function signature => ok
def y(a, b, c, d, e, f):
    pass


# Hanging indentation in function signature => ok
def z(
        a, b, c, d, e, f):
    pass


# No indentation in list literal => ok
a = ['a', 'b', 'c']

# Hanging indentation in list literal => ok
b = [
    'a', 'b', 'c',
]

# Visual indentation in list literal => warning
a = ['a', 'b', 'c',
     'd']

# Visual indentation in nested dict/list literals => warning
c = {
    "x": ["a", "b",
          "c", "d"],  # warning
    "y": "1",
    "xyz": {"a": "b",
            "c": ["d", "e",  # warning
                  "f", "g"]},  # warning
}

# Hanging indentation in tuple literal => ok
a = (
    "xdshjfd(fashugifhg",
    ")(dfsafa[",
    "'(",
    'xdshjfd(fashugifhg',
    ')(dfsafa[',
    '"(',
    """
    fdahfdalvc,(fdsajdfkasg[
    sdhjfdslahfs[""",
)

# Hanging-indented lines incidentally aligned with an opening bracket => ok
d = {
    a: [
        "x", "y", "z",  # ok
    ],
}

# Hanging indentation in 'if' condition => ok
if (a and y
        and z):
    """This used to be a false positive until MLD-45 had been fixed."""
    pass


def test_flake8_novisualindent():
    """Test if the plugin catches all Q900 violations in this file (and no more)."""
    out, __ = subprocess.Popen(["flake8", __file__], stdout=subprocess.PIPE).communicate()
    lines = [line for line in out.split("\n") if "Q900" in line]
    expected_locations = [
        (6, 7),
        (31, 6),
        (36, 11),
        (39, 13),
        (40, 19),
    ]
    assert lines == [
        "{path}:{line}:{column}: Q900 Visual indent is discouraged.".format(
            path=__file__,
            line=line,
            column=column,
        ) for line, column in expected_locations
    ]
