"""flake8 plugin to warn about visual indentation."""

# We have chosen this violation code because it is detected by diff-quality and it does
# not collide with any codes of flake8 or plugins we use or are considering to use in the
# future.
VIOLATION_CODE = "Q900"

OPENING_BRACKETS = {"(", "[", "{"}


def check_novisualindent(logical_line, tokens):
    """Yield warnings for each location where visual indentation is used.

    Args:
        logical_line:
            The logical line. This argument is not used, but actually required to make
            flake8 call this function for each logical line.
        tokens:
            The list of tokens. Each token is a 5-tuple containing information about a
            token within a line. This plugin uses a token's second (its string
            representation) and third element (its location := line, column).

    Yields:
        Tuples of ((line, column), message) where message is always
        "Q900 Visual indent is discouraged" and (line, column) indicate the location
        of the visual indentation in the indented line.
    """
    visual_indent_columns = set()
    newline = False

    for __, token_str, location, __, __ in tokens:
        column = location[1]
        if newline:
            # First token after newline.
            # Check if the token is aligned to any opening bracket.
            newline = False
            if column in visual_indent_columns:
                yield location, "{violation_code} Visual indent is discouraged.".format(
                    violation_code=VIOLATION_CODE,
                )
            visual_indent_columns.clear()
        if token_str in OPENING_BRACKETS:
            # Memoize positions after opening brackets to detect visual indentation.
            visual_indent_columns.add(column + 1)
        elif token_str == '\n':
            # Make a note to check the first token of the next line for visual indentation.
            newline = True
            # If an opening bracket directly precedes a newline, it must be followed by
            # hanging indentation, so it's not considered when checking alignment.
            if column in visual_indent_columns:
                visual_indent_columns.remove(column)


check_novisualindent.name = "novisualindent"
check_novisualindent.version = "0.0.2"
